Twitterbootstrap is a TYPO3 extension which configures TYPO3 to use
http://twitter.github.com/bootstrap/ for websites built with TYPO3.