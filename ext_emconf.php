<?php

########################################################################
# Extension Manager/Repository config file for ext "default".
#
# Auto generated 30-05-2010 14:06
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Twitter Bootstrap',
	'description' => 'Twitter Bootstrap is a TYPO3 extension which configures TYPO3 to use http://twitter.github.com/bootstrap/ for websites built with TYPO3.',
	'category' => 'misc',
	'author' => 'Andreas Otto',
	'author_email' => 'andreas@otto-hanika.de',
	'shy' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '4.5.0-4.7.100',
			'cms' => '',
			'css_styled_content' => '',
			'gridelements' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => '',
	'suggests' => array(
	),
);

?>