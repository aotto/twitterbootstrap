page = PAGE
page {

	typeNum = 0

	includeJS {

	}

	includeJSFooter {
		default001 = EXT:twitterbootstrap/Resources/Public/bootstrap/js/bootstrap.js
		default002 = EXT:twitterbootstrap/Resources/Public/fancybox/jquery.fancybox.js
		default003 = EXT:twitterbootstrap/Resources/Public/fancybox/helpers/jquery.fancybox-buttons.js
		default004 = EXT:twitterbootstrap/Resources/Public/fancybox/helpers/jquery.fancybox-media.js
		default005 = EXT:twitterbootstrap/Resources/Public/fancybox/helpers/jquery.fancybox-thumbs.js
		default006 = EXT:twitterbootstrap/Resources/Public/JavaScript/jquery.fancybox-settings.js
	}

	includeCSS {
		default001 = EXT:twitterbootstrap/Resources/Public/bootstrap/css/bootstrap.css
		default002 = EXT:twitterbootstrap/Resources/Public/Css/bootstrap-body.css
		default004 = EXT:twitterbootstrap/Resources/Public/fancybox/jquery.fancybox.css
		default005 = EXT:twitterbootstrap/Resources/Public/fancybox/helpers/jquery.fancybox-buttons.css
		default006 = EXT:twitterbootstrap/Resources/Public/fancybox/helpers/jquery.fancybox-thumbs.css
		default007 = EXT:twitterbootstrap/Resources/Public/Css/fancybox-settings.css
	}

	10 = FLUIDTEMPLATE
	10 {
		file = EXT:twitterbootstrap/Resources/Private/Templates/Fixed.html
		partialRootPath = EXT:twitterbootstrap/Resources/Private/Partials/
		layoutRootPath = EXT:twitterbootstrap/Resources/Private/Layouts/
	}
}


[globalVar = LIT:fluid = {$twitterbootstrap.layout.type}]
page {
	includeCSS {
		default003 = EXT:twitterbootstrap/Resources/Public/bootstrap/css/bootstrap-responsive.css
	}

	meta {
		viewport = width=device-width, initial-scale=1.0
	}

	10 {
		file = EXT:twitterbootstrap/Resources/Private/Templates/Fluid.html
	}
}
[global]